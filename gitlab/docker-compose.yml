version: '3.3'
services:
  gitlab:
    image: 'gitlab/gitlab-ce:latest'
    container_name: gitlab
    restart: always
    labels:
      - traefik.enable=true
      - traefik.docker.network=traefik-public
      - traefik.constraint-label=traefik-public
      - traefik.http.routers.gitlab-http.rule=Host(`gitlab.example.com`)
      - traefik.http.routers.gitlab-http.entrypoints=http
      - traefik.http.routers.gitlab-https.rule=Host(`gitlab.example.com`)
      - traefik.http.routers.gitlab-https.entrypoints=https
      - traefik.http.routers.gitlab-https.tls=true
      - traefik.http.routers.gitlab-https.tls.certresolver=le
      - traefik.http.services.gitlab.loadbalancer.server.port=80
      - traefik.http.routers.gitlab-http.middlewares=https-redirect
      - traefik.http.middlewares.https-redirect.redirectscheme.scheme=https
      - traefik.http.middlewares.https-redirect.redirectscheme.permanent=true
      - traefik.tcp.routers.gitlab-ssh.rule=HostSNI(`*`)
      - traefik.tcp.routers.gitlab-ssh.entrypoints=ssh
      - traefik.tcp.routers.gitlab-ssh.service=gitlab-ssh-svc
      - traefik.tcp.services.gitlab-ssh-svc.loadbalancer.server.port=22
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        # Server Configuration
        external_url 'https://gitlab.example.com'
        nginx['listen_https'] = false
        nginx['listen_port'] = 80
        nginx['proxy_set_headers'] = {
          "X-Forwarded-Proto" => "https",
          "X-Forwarded-SSL" => "on"
        }
        # Database Configuration
        gitlab_rails['db_adapter'] = "postgresql"
        gitlab_rails['db_database'] = "gitlab"
        gitlab_rails['db_username'] = "postgres"
        gitlab_rails['db_password'] = "password"
        gitlab_rails['db_host'] = "gitlab-postgres"
        
        # Gitlab Registry
        registry['enable'] = false
        gitlab_rails['registry_enabled'] = true
        gitlab_rails['registry_host'] = "registry.gitlab.example.com"                      
        gitlab_rails['registry_api_url'] = "https://registry.gitlab.example.com"    
        gitlab_rails['registry_issuer'] = "gitlab-issuer"
 
        # GitLab SSH
        gitlab_rails['gitlab_shell_ssh_port'] = 22
    volumes:
      - './config:/etc/gitlab'
      - './logs:/var/log/gitlab'
      - './data:/var/opt/gitlab'
    networks:
      - traefik-public
    depends_on:
      - gitlab-postgres
 
  registry:
    restart: unless-stopped
    image: registry:2.7
    container_name: gitlab_registry
    volumes:
     - ./data:/registry
     - ./certs:/certs
    labels:
      - traefik.enable=true
      - traefik.docker.network=traefik-public
      - traefik.constraint-label=traefik-public
      - traefik.http.routers.registry-http.rule=Host(`registry.gitlab.example.com`)
      - traefik.http.routers.registry-http.entrypoints=http
      - traefik.http.routers.registry-https.rule=Host(`registry.gitlab.example.com`)
      - traefik.http.routers.registry-https.entrypoints=https
      - traefik.http.routers.registry-https.tls=true
      - traefik.http.routers.registry-https.tls.certresolver=le
      - traefik.http.services.registry.loadbalancer.server.port=5000
      - traefik.http.routers.registry-http.middlewares=https-redirect
      - traefik.http.middlewares.https-redirect.redirectscheme.scheme=https
      - traefik.http.middlewares.https-redirect.redirectscheme.permanent=true
    networks:
      - traefik-public
    environment:
      REGISTRY_LOG_LEVEL: debug
      REGISTRY_STORAGE_FILESYSTEM_ROOTDIRECTORY: /registry
      REGISTRY_AUTH_TOKEN_REALM: https://gitlab.example.com/jwt/auth
      REGISTRY_AUTH_TOKEN_SERVICE: container_registry
      REGISTRY_AUTH_TOKEN_ISSUER: gitlab-issuer
      REGISTRY_AUTH_TOKEN_ROOTCERTBUNDLE: /certs/gitlab-registry.crt
      REGISTRY_STORAGE_DELETE_ENABLED: 'true' 
 
  gitlab-postgres:
    image: postgres:13
    restart: always
    environment:
      POSTGRES_PASSWORD: "password"
      POSTGRES_USER: postgres
      POSTGRES_DB: gitlab
    volumes:
      - './database:/var/lib/postgresql/data'
    networks:
      - traefik-public
 
networks:
  traefik-public:
    external: true

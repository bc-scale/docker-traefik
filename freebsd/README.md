## Prerequisites
- https://github.com/BastilleBSD/bastille

```
su
```

```
pkg update && \
pkg install -y bastille git nano && \
sysrc bastille_enable=YES
```

```
sysrc cloned_interfaces+=lo1 && \
sysrc ifconfig_lo1_name="bastille0" && \
service netif cloneup
```

```
ifconfig && \
nano /etc/pf.conf
```

```
ext_if="${}"

set block-policy return
scrub in on $ext_if all fragment reassemble
set skip on lo

table <jails> persist
nat on $ext_if from <jails> to any -> ($ext_if:0)
rdr-anchor "rdr/*"
rdr pass on $ext_if proto tcp from any to any port 80 -> 192.168.10.10 port 80
rdr pass on $ext_if proto tcp from any to any port 443 -> 192.168.10.10 port 443

block in all
pass out quick keep state
antispoof for $ext_if inet
pass in inet proto tcp from any to any port ssh flags S/SA keep state
```

```
sysrc pf_enable=YES
service pf start
```

```
bastille bootstrap 13.0-RELEASE update
bastille create traefik 13.0-RELEASE 192.168.10.10
```

```
bastille bootstrap https://gitlab.com/bastillebsd-templates/traefik
bastille template traefik bastillebsd-templates/traefik
```

#

```
bastille console traefik
```

```
rm /usr/local/etc/traefik.yml && \
rm /usr/local/etc/traefik.toml
```

```
nano /usr/local/etc/traefik.yml
```


```
api:
  dashboard: true

certificatesResolvers:
  le:
    acme:
      email: "hello@example.com"
      storage: "/etc/traefik/acme.json"
      tlsChallenge: {}

entryPoints:
  http:
    address: ":80"                            
    http:
      redirections:                           
        entryPoint:
          to: "websecure"
          scheme: "https"
  https:
    address: ":443"

global:
  checknewversion: true
  sendanonymoususage: true

providers:
  docker:
    endpoint: "unix:///var/run/docker.sock"
    exposedByDefault: false
    network: "traefik-public"       
  file:
    filename: "/etc/traefik/routes.yml"
    watch: true
```

```
mkdir -p /etc/traefik && \
touch /etc/traefik/routes.yml && \
touch /etc/traefik/acme.json && chmod 600 /etc/traefik/acme.json
```

```
nano /etc/traefik/routes.yml
```
```
http:
  routers:
    dashboard-http:
      entryPoints:
      - http
      rule: "Host(`traefik.example.com`)"
      service: api@internal
    dashboard-https:
      entryPoints:
      - https
      rule: "Host(`traefik.example.com`)"
      service: api@internal
      tls:
        certResolver: le
```

```
crontab -e
```
```
@reboot /usr/local/bin/traefik --configFile=/usr/local/etc/traefik.yml
```
